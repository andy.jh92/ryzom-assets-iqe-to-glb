import shutil
import subprocess
import os

FORCE = True

def compile_iqe_to_iqm(input, output):
    if not os.path.exists(output):
        create_folders_for_file(output)
        subprocess.check_output("dependencies\\iqm.exe --forcejoints " + output + " " + input)
        print(output)


def convert_iqm_to_glb(input, output):
    if not os.path.exists(output) or FORCE:
        create_folders_for_file(output)
        cmd = "python dependencies\\iqm2glb\\iqm2glb.py " + input + " " + output
        print(cmd)
        subprocess.check_output(cmd)
        print(output)

def copy_file(input, output):
    if not os.path.exists(output):
        create_folders_for_file(output_file_path)
        shutil.copy(file_path, output_file_path)
        print(output)

def replace_root(path, root_source, root_target):
    path = path[len(root_source):]
    return root_target + path


def create_folders_for_file(file_path):
    directory_path = os.path.dirname(file_path)
    if not os.path.exists(directory_path):
        os.makedirs(directory_path, exist_ok=True)



def replace_suffix(file_name, target_suffix):
    last_index_of_dot = file_name.rfind('.')
    return file_name[:last_index_of_dot] + target_suffix


if __name__ == '__main__':
    for root, dirs, files in os.walk("dependencies\\ryzom-iqe-assets"):

        for file_name in files:
            if not file_name.startswith("bat_a_01.iqe"):
                continue

            file_path = os.path.join(root, file_name)
            if file_path.endswith(".iqe"):

                iqm_root = replace_root(root, "dependencies\\ryzom-iqe-assets", "temporary")
                iqm_file_name = replace_suffix(file_name, ".iqm")
                iqm_file_path = os.path.join(iqm_root, replace_suffix(file_name, ".iqm"))
                output_root = replace_root(root, "dependencies\\ryzom-iqe-assets", "output")
                output_file_name = replace_suffix(file_name, ".glb")
                output_file_path = os.path.join(output_root, output_file_name)

                compile_iqe_to_iqm(file_path, iqm_file_path)
                convert_iqm_to_glb(iqm_file_path, output_file_path)

            else:
                output_file_path = replace_root(file_path, "dependencies\\ryzom-iqe-assets", "output")
                copy_file(file_path, output_file_path)
