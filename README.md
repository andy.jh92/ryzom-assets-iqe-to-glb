# About
Here are scripts that convert the ryzom assets from iqe to glb.

# Prerequisites
* Use Windows.
* Download the latest iqm.exe from https://github.com/lsalzman/iqm and put it into the .\dependencies\ folder.

# TODO
* Handle special material attributes like doublesided in iqm2glb
* Rotate models